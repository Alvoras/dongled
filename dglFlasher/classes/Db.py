import mysql.connector

DB_USER = "root"
DB_PWD = "toor"
DB_NAME = "workin-space"

class Db:
    conn = None
    cursor = None

    def __init__(self):
        try:
            self.conn = mysql.connector.connect(host="localhost", user=DB_USER, password=DB_PWD, database=DB_NAME)
            self.cursor = self.conn.cursor(prepared=True)
        except Exception as e:
            raise Exception("[+] Failed to open database connection")

    def fetch_key(self, rental_id):
        query = "SELECT rent_token FROM rent WHERE id_rent = {}".format(rental_id)
        try:
            self.cursor.execute(query)
            rows = self.cursor.fetchone()
        except Exception as e:
            raise Exception("[+] Failed fetch key from database")
        finally:
            if self.cursor:
                self.cursor.close()

        return rows[0].decode("utf-8")
