from sys import exit, argv
from subprocess import Popen, PIPE

from os import getenv

from hashlib import sha256

from modules.toolbox import *
from classes.Db import *

if __name__ == '__main__':
    template_path = "fware/template/template.ino"
    patched_path = "fware/patched/patched.ino"
    # board = "sparkfun:avr:promicro"
    patched_ino = "fware/patched/patched.ino"
    upload_cmd = "arduino --upload {} --port {}"
    key_marker = "!!KEY!!"

    rental_id = argv[1];
    rental_len = len(rental_id)
    full_rental_id = "00000000"
    full_rental_id = full_rental_id[:-rental_len]
    full_rental_id = full_rental_id + str(rental_id)

    ports = serial_ports()

    dongle_port = find_dongle(ports)

    if not dongle_port:
        print("[+] Aucun dongle trouvé.")
        exit()

    try:
        db = Db()
    except Exception as e:
        print(e)
        exit()

    auth_key = db.fetch_key(rental_id)
    full_auth_info = "{}:{}".format(full_rental_id, auth_key)
    # wrapper_auth_key = sha256(auth_key).hexdigest()

    print("[>] Key : {}".format(auth_key))
    print("[>] Rental id : {}".format(full_rental_id))

    print(full_auth_info)
    
    with open(template_path, "r") as template:
        template_data = template.read()
        template.close()

    patched_template = template_data.replace(key_marker, full_auth_info)

    with open(patched_path, "w") as patched:
        patched.write(patched_template)
        patched.close()

    print("[+] Flashing...")

    out = Popen(upload_cmd.format(patched_ino, dongle_port), shell=True, stdout=PIPE)
    out.communicate()

    print("[+] Done")

# Read user id
# Get key from db from the "rentals" table's line
# Create .ino file from template
# Find the dongle
# Flash it
# Done !
