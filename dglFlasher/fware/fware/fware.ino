#define ASLEEP 0
#define AWAKE 1

#define HELLO_PKT "HLO"
#define LISTEN_PKT "LSN"
#define KEY_PKT "KEY"
#define PKT_TIMEOUT 1

bool _state = ASLEEP;

boolean recvInProgress = false;
byte index = 0;

bool endRcv = false;

char startMarker = '<';
char endMarker = '>';
char rcvd;

const byte bufSize = 32;
char *bufStr = NULL;

void setup() {
  bufStr = malloc(sizeof(char) * bufSize);
  Serial.begin(9600);
}

void loop() {
  recvPacket();
}
/*
void recvWithStartEndMarkers() {
  boolean recvInProgress = false;
  byte index = 0;

  bool endRcv = false;

  char startMarker = '<';
  char endMarker = '>';
  char rcvd;

  const byte bufSize = 32;
  char *bufStr = NULL;
  bufStr = malloc(sizeof(char) * bufSize);

  while (Serial.available() > 0 && endRcv == false) {
    rcvd = Serial.read();

    if (recvInProgress == true) {
      // If the read char is not the end marker
      // Then store
      if (rcvd != endMarker) {
        bufStr[index] = rcvd;
        index++;
        if (index >= bufSize) {
          index = bufSize - 1;
        }
      }
      else {
        bufStr[index] = 0x00; // terminate the string
        recvInProgress = false;
        index = 0;
        endRcv = true;
        Serial.print(bufStr);
      }
    }

    else if (rcvd == startMarker) {
      recvInProgress = true;
    }
  }
}
*/
void recvPacket() {
  while (Serial.available() > 0 && endRcv == false) {
    rcvd = Serial.read();

    if (recvInProgress == true) {
      if (rcvd != endMarker) {
        bufStr[index] = rcvd;
        index++;
        if (index >= bufSize) {
          index = bufSize - 1;
        }
      } else {
        bufStr[index] = 0x00; // terminate the string
        recvInProgress = false; // Signal we found the end marker
        index = 0; // Reset the index
        endRcv = true; // Signal the end of the receiving state

        handlePackets(bufStr);
      }
    } else if (rcvd == startMarker) {
      recvInProgress = true;
    }
  }
}

void handlePackets(char *pkt) {
  switch (_state) {
    case ASLEEP:
      // Wait for hello
      if (strcmp(HELLO_PKT, pkt) == 0) {
        sendListen();
        _state = AWAKE;
      }
      break;
    case AWAKE:
      // Wait for key
      if (strcmp(KEY_PKT, pkt) == 0) {
        sendKey();
        _state = ASLEEP;
      }else{
        _state = ASLEEP;
      }
      break;
    default:
      cleanseStr(pkt, strlen(pkt));
      _state = ASLEEP;
  }

  endRcv = false;
}

char *wrapPacket(char *pkt) {
  char *buf = NULL;
  buf = malloc(sizeof(char) * 6);

  sprintf(buf, "<%s>", pkt);

  return buf;
}

void sendKey() {
  char key[42] = "00000040:4f61bfdb70032d93d6f8b3920aebfd88";
  key[42] = '\0';

  Serial.write(key);
}

void sendListen() {
  Serial.write(wrapPacket(LISTEN_PKT));
}

void cleanseStr(char *str, int strLen) {
  int i = 0;
  for (i = 0; i < strLen; i++) {
    str[i] = 0x00;
  }
}


