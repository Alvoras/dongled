import serial.tools.list_ports as list_ports
import serial
from time import sleep

HELLO_PKT = "HLO"
LISTEN_PKT = "LSN"
HELLO_TIMEOUT = 0.5

def wrap_packet(packet):
    return '<%s>' % packet


def serial_ports():
    # python -m serial.tools.list_ports

    ports_list = list(list_ports.comports())

    result = []
    for port in ports_list:
        result.append(port.device)

    return result


def find_dongle(portList):
    hello = wrap_packet(HELLO_PKT)
    reply = wrap_packet(LISTEN_PKT)
    res = ''
    dongle_port = ''
    loop_cnt = 0
    break_for = False

    for port in portList:
        print("[+] Scanning %s..." % port)

        try:
            handle = serial.Serial(
                port='\\\\.\\%s' % port,
                baudrate=9600,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=HELLO_TIMEOUT,
                writeTimeout=0.5,
                dsrdtr=True
            )
        except Exception as e:
            print("[+] Failed to open port %s" % port)
            continue

        try:
            if handle.isOpen():
                print("[+] Port is open. Reopening it...")
                handle.close()
        except Exception as e:
            print("[+] Failed to open port %s" % port)
            continue

        try:
            handle.open()
        except Exception as e:
            print("[+] Failed to open port %s" % port)
            continue

        print("[+] Sending hello...")

        # print(hello.encode())

        loop_cnt = 0

        while loop_cnt < 3:
            sleep(0.5)

            try:
                handle.write(hello.encode())

                try:
                    print("[+] Waiting reply... (timeout = {})".format(HELLO_TIMEOUT))

                    try:
                        sleep(HELLO_TIMEOUT)

                        while handle.inWaiting() > 0:
                            res = handle.read_all()
                    except Exception as e:
                        print(e)

                    if res:
                        if res.decode() == reply:
                            print("[+] Found dongle at %s" % port)
                            dongle_port = port
                            break_for = True
                            break
                    else:
                        print("[+] Got nothing back... (%d out of 3)" % loop_cnt)
                        loop_cnt += 1

                except Exception as e:
                    print(e)
                    loop_cnt += 1

            except Exception as e:
                print(e)
                loop_cnt += 1

        if break_for:
            break

    return dongle_port
