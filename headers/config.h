#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#define DB_HOST "localhost"
#define DB_USER "root"
#define DB_PWD "toor"
#define DB_NAME "workin-space"


#define HELLO_PKT "HLO"
#define LISTEN_PKT "LSN"
#define KEY_PKT "KEY"

#define HELLO_TIMEOUT 1
#define KEY_TIMEOUT 1
#define MAIN_LOOP_TIMEOUT 10

#define TRY_CONNECT "scripts\\reconnect.bat"
#define DISCONNECT "scripts\\disconnect.bat"
#define IS_CONNECTED "scripts\\is_connected.bat"

#define NETWORK_DOWN 0
#define NETWORK_UP 1

#define ERR_UNEXPECTED -1
#define ERR_NOT_UNIQ -2
#define ERR_NOT_FOUND -3
#define DONGLE_FOUND 1

#define AUTH_KEY 0

#endif // CONFIG_H_INCLUDED
