#ifndef DONGLED_H_INCLUDED
#define DONGLED_H_INCLUDED

HANDLE getDongleHandle(int *helloResult);
HANDLE createCOMHandle(char *COMNo);
char **getCOMPortNo(char *COMList, int *portQty);
void setCOMHandle(HANDLE *hSerial);
HANDLE broadcastHello(char **COMNo, int portQty, int *helloResult);
char *wrapPacket(char *pkt);
char *getKeyFromDongle(HANDLE dongle);
int isConnected();
char *parsePacket();

#endif // DONGLED_H_INCLUDED
