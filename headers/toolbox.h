#ifndef TOOLBOX_H_INCLUDED
#define TOOLBOX_H_INCLUDED

#include <time.h>

void *smalloc(int size, int qty);
void *scalloc(int size, int qty);
void *srealloc(void *ptr, int size);
FILE *spopen(char *str, char *mode);
char *execCmd(char *cmd);
int countLines(char *text);
char **splitStr(char *str, char *delimiter, int *cnt);
void dbg();
float getSeconds(clock_t start, clock_t end);

#endif // TOOLBOX_H_INCLUDED
