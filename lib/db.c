#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <winsock.h>
#include <MYSQL/mysql.h>

#include "../headers/toolbox.h"
#include "../headers/config.h"
#include "../headers/dongled.h"
#include "../headers/db.h"

extern MYSQL db;

int initSql()
{
    mysql_init(&db);

    mysql_options(&db, MYSQL_READ_DEFAULT_GROUP, "option");
    if(mysql_real_connect(&db, DB_HOST, DB_USER, DB_PWD, DB_NAME, 0, NULL, 0))
    {
        printf("[+] Connexion etablie\n");
        return 1;
    }
    else
    {
        printf("[!] Erreur de connexion a la bdd\n");
        return 0;
    }
}

char *fetchAuthKey(int rentalId){
  MYSQL_RES *result = NULL;
  MYSQL_ROW row;
  char *key = NULL;
  key = smalloc(sizeof(char), 33);

  // char *query = "SELECT auth_key FROM auth_key WHERE rental_id=%s";
  // char *query = "SELECT auth_key FROM auth";
  char *query = "SELECT rent_token FROM rent WHERE id_rent = %d";

  char *finalQuery = NULL;
  finalQuery = smalloc(sizeof(char), strlen(query));

  sprintf(finalQuery, query, rentalId);

  mysql_query(&db, finalQuery);
  // mysql_query(&db, query);

  result = mysql_use_result(&db);

  if( (row = mysql_fetch_row(result)) ){
    strcpy(key, row[AUTH_KEY]);
  }else{
     printf("[!] Failed to fetch MySql rows");
     key[0] = 0x00;
  }

  mysql_free_result(result);

  return key;
}
