#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../headers/toolbox.h"
#include "../headers/config.h"
#include "../headers/dongled.h"

HANDLE getDongleHandle(int *helloResult){
        int portQty = 0;
        int i = 0;

        HANDLE dongleHandle;

        char *COMList = NULL;
        char **COMNo = NULL;

        COMList = execCmd("mode|findstr \"COM\"");

        COMNo = getCOMPortNo(COMList, &portQty);

        dongleHandle = broadcastHello(COMNo, portQty, helloResult);

        free(COMList);

        for (i = 0; i < portQty; i++) {
            free(COMNo[i]);
        }

        free(COMNo);

        return dongleHandle;
}

HANDLE broadcastHello(char **COMNo, int portQty, int *helloResult) {
        clock_t startTime = 0;
        clock_t endTime = 0;
        float duration = 0;

        int i = 0;
        int handleIndex = -1;
        int timeout = 0;
        int dongleCnt = 0;
        HANDLE *handleList = NULL;
        handleList = smalloc(sizeof(HANDLE), portQty);

        int helloSize = 5;
        char *hello = NULL;
        hello = wrapPacket(HELLO_PKT);
        hello[helloSize] = 0x00;

        long unsigned int written = 0;

        long unsigned int rcvdCnt = 0;
        short rcvdLen = 5+1;
        char* rcvd = NULL;

        char *parsedRcvdPacket = NULL;

        rcvd = smalloc(sizeof(char), rcvdLen);

        for (i = 0; i < portQty; i++) {
                // createCOMHandle() from COMNo list
                // Set COM handle with setCOMHandle(&hSerial);
                // Send hello
                // Wait for answer
                // Timeout 1 sec
                // If got answer then validate answer against awaited value
                // If validated then store the value and keep checking
                handleList[i] = NULL;
                handleList[i] = createCOMHandle(COMNo[i]);

                if (handleList[i] == INVALID_HANDLE_VALUE) {
                  printf("[+] Failed to create serial file handle (COM%s)\n", COMNo[i]);
                  break;
                }

                setCOMHandle(&handleList[i]);

                printf("[+] Testing COM%s...\n", COMNo[i]);

                startTime = clock();
                if(!WriteFile(handleList[i], hello, helloSize, &written, NULL))
                {
                        printf("[!] Error while writing (COM%s)\n", COMNo[i]);
                        CloseHandle(handleList[i]);
                }

                while (rcvdCnt == 0 && !timeout) {
                        ReadFile(handleList[i], rcvd, sizeof(char)*rcvdLen, &rcvdCnt, NULL);
                        endTime = clock();
                        duration = getSeconds(startTime, endTime);

                        if (duration > HELLO_TIMEOUT) {
                                timeout = 1;
                                printf("[+] COM%s timeout\n", COMNo[i]);
                        }
                }

                // Terminate the string
                rcvd[rcvdCnt] = 0x00;

                //sscanf(rcvd, "<%s>", parsedRcvdPacket);

                if (rcvdCnt > 0) {
                  parsedRcvdPacket = parsePacket(rcvd);
                  if (strcmp(parsedRcvdPacket, LISTEN_PKT) == 0) {
                          printf("[+] Dongle found (COM%s) !\n", COMNo[i]);
                          handleIndex = i;
                          dongleCnt++;
                  }
                }

                timeout = 0;
        }

        // If counter > 1 then error
        if (dongleCnt == 1) {
                // Return handle
                printf("[+] Selected dongle : COM%s\n", COMNo[handleIndex]);
                *helloResult = DONGLE_FOUND;

                // If success return the handle, else let the program run and return NULL
                return handleList[handleIndex];
        }else if (dongleCnt == 0) {
                // Return nothing found
                *helloResult = ERR_NOT_FOUND;
        }else if(dongleCnt > 1) {
                // Return more than 1
                *helloResult = ERR_NOT_UNIQ;
        }else{
                // Return unexpected error
                printf("[!] Error\n");
                *helloResult = ERR_UNEXPECTED;
        }

        free(handleList);
        free(parsedRcvdPacket);

        // If all fail, return NULL as an error signal
        return NULL;
}

char **getCOMPortNo(char *COMList, int *portQty){
        int i = 0;
        int COMNoLen = 0;

        char **COMNoList = NULL;
        char **bufList = NULL;
        char *bufPtr = NULL;

        bufList = splitStr(COMList, "\n", portQty);
        COMNoList = smalloc(sizeof(char*), *portQty);
        for (size_t i = 0; i < *portQty; i++) {
                // Max COM99
                COMNoList[i] = smalloc(sizeof(char), 3);
        }

        for (i = 0; i < *portQty; i++) {
                bufPtr = strstr(bufList[i], "COM")+3;
                COMNoLen = strlen(bufPtr) - 1;
                strncpy(COMNoList[i], bufPtr, COMNoLen);
                COMNoList[i][COMNoLen] = 0x00;
        }

        for (i = 0; i < *portQty; i++) {
                free(bufList[i]);
        }
        free(bufList);

        free(bufPtr);

        return COMNoList;
}

HANDLE createCOMHandle(char *COMNo){
        HANDLE hSerial;
        char COMStringBase[] = "\\\\.\\COM";

        char *COMPort = NULL;
        COMPort = strcat(COMStringBase, COMNo);

        hSerial = CreateFile(
                COMPort, GENERIC_READ|GENERIC_WRITE, 0, NULL,
                OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

        // Creation fail must be handled in the caller function as it triggers a break
        return hSerial;
}


void setCOMHandle(HANDLE *hSerial){
        DCB dcbSerialParams = {0};
        COMMTIMEOUTS timeouts = {0};

        // Set device parameters (57600 baud, 1 start bit,
        // 1 stop bit, no parity)
        dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
        if (GetCommState(*hSerial, &dcbSerialParams) == 0)
        {
                printf("[!] Error getting device state\n");
                CloseHandle(*hSerial);
        }

        dcbSerialParams.BaudRate = CBR_9600;
        dcbSerialParams.ByteSize = 8;
        dcbSerialParams.StopBits = ONESTOPBIT;
        dcbSerialParams.Parity = NOPARITY;
        if(SetCommState(*hSerial, &dcbSerialParams) == 0)
        {
                printf("[!] Error setting device parameters\n");
                CloseHandle(*hSerial);
        }

        // Set COM port timeout settings
        timeouts.ReadIntervalTimeout = 50;
        timeouts.ReadTotalTimeoutConstant = 50;
        timeouts.ReadTotalTimeoutMultiplier = 10;
        timeouts.WriteTotalTimeoutConstant = 50;
        timeouts.WriteTotalTimeoutMultiplier = 10;
        if(SetCommTimeouts(*hSerial, &timeouts) == 0)
        {
                printf("[!] Error setting timeouts\n");
                CloseHandle(*hSerial);
        }
}

char *getKeyFromDongle(HANDLE dongle){
  clock_t startTime = 0;
  clock_t endTime = 0;
  float duration = 0;
  int timeout = 0;

  int keyPktSize = 5;
  char *getKey = NULL;
  getKey = wrapPacket(KEY_PKT);
  getKey[keyPktSize] = 0x00;

  long unsigned int written = 0;

  long unsigned int rcvdCnt = 0;
  short rcvdLen = 41+1;
  char* rcvd = NULL;

  rcvd = smalloc(sizeof(char), rcvdLen);

  startTime = clock();
  if(!WriteFile(dongle, getKey, keyPktSize, &written, NULL))
  {
          printf("[!] Error while fetching the key from the dongle\n");
          CloseHandle(dongle);
  }

  while (rcvdCnt == 0 && !timeout) {
          ReadFile(dongle, rcvd, sizeof(char)*rcvdLen, &rcvdCnt, NULL);
          endTime = clock();
          duration = getSeconds(startTime, endTime);

          if (duration > KEY_TIMEOUT) {
                  timeout = 1;
                  printf("[!] Timeout while fetching the key from the dongle");
          }
  }

  // Terminate the string
  rcvd[rcvdCnt] = 0x00;

  return rcvd;
}

char *wrapPacket(char *pkt){
  char *buf = NULL;
  buf = smalloc(sizeof(char), 6);

  sprintf(buf, "<%s>", pkt);

  return buf;
}

int isConnected(){
  return execCmd(IS_CONNECTED)[0];
}

char *parsePacket(char* pkt){
  char *buf = NULL;
  int i = 0;
  buf = smalloc(sizeof(char), 4);

  if (pkt[0] != '<') {
    return NULL;
  }

  // WARNING
  // i START AT 1 HERE
  // IT IS DONE ON PURPOSE TO AVOID THE '<' THAT SIGNAL THE START OF THE PACKET
  while (pkt[i++] != '>') {
    buf [i-1] = pkt[i];
  }

  buf[3] = 0x00;

  return buf;
}
