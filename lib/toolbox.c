#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../headers/toolbox.h"

void *smalloc(int size, int qty) {
    void *ret = NULL;

    if ((ret = malloc(size * qty)) == NULL) {
        printf("[!] Malloc failed (%d*%d)\n", size, qty);
        exit(EXIT_FAILURE);
    }

    return ret;
}

void *srealloc(void *ptr, int size) {
    void *ret = NULL;

    if ((ret = realloc(ptr, size)) == NULL) {
        printf("[!] Realloc failed (%d)\n", size);
        exit(EXIT_FAILURE);
    }

    return ret;
}

void *scalloc(int size, int qty){
  void *ret = NULL;

  if ((ret = calloc(qty, size)) == NULL) {
      printf("[!] Calloc failed (%d*%d)\n", qty, size);
      exit(EXIT_FAILURE);
  }

  return ret;
}

FILE *spopen(char *str, char *mode){
  FILE *fd = NULL;
  fd = popen(str, mode);

  if (!fd){
    printf("[!] Popen failed (%s, %s)\n", str, mode);
    exit(EXIT_FAILURE);
  }
  return fd;
}

char *execCmd(char *cmd){
  unsigned int allocLen = 256;
  unsigned int outputLen = 0;
  unsigned long outputRead = 0;

  char *rtn = NULL;
  rtn = scalloc(sizeof(char), allocLen);

  char buffer[256];

  FILE *fd = NULL;
  fd = spopen(cmd, "r");

  while( (outputRead = fread(buffer, 1, sizeof(buffer), fd)) != 0){
    if (outputLen + outputRead >= allocLen) {
      allocLen *= 2;
      rtn = realloc(rtn, allocLen);
    }

    strncpy(rtn + outputLen, buffer, outputRead);
    outputLen += outputRead;
  }

  rtn[outputLen] = 0x00;

  pclose(fd);
  return rtn;
}

int countLines(char *text){
  int i = 0;
  int cnt = 0;

  while (text[i] != 0x00) {
    if (text[i] == '\n') cnt++;
    i++;
  }

  return cnt;
}

// Variadic option is used to update optional extern counter
char **splitStr(char *str, char *delimiter, int *cnt) {
    char * token      = NULL;
    char **tokenArray = NULL;
    int    count      = 0;

    token         = strtok(str, delimiter); // Get the first token
    tokenArray    = scalloc(sizeof(char **), 1);

    if (!token) {
      *cnt = count;
      return tokenArray;
    }

    while (token != NULL) { // While valid tokens are returned
        tokenArray[count] = strdup(token);
        count++;
        tokenArray = srealloc(tokenArray, sizeof(char **) * (count + 1));
        token      = strtok(NULL, delimiter); // Get the next token
      }

    tokenArray[count] = 0x00;

    *cnt = count;

    free(token);

    return tokenArray;
}

void dbg() {
  printf("dbg\n");
}

float getSeconds(clock_t start, clock_t end){
  return (float)(end - start) / CLOCKS_PER_SEC;
}
