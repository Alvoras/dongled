#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <MYSQL/mysql.h>

#include "headers/toolbox.h"
#include "headers/config.h"
#include "headers/dongled.h"
#include "headers/db.h"

MYSQL db;

int main(int argc, char const *argv[]) {
        int networkState = NETWORK_DOWN;
        char *key = NULL;

        int splitCnt = 0;

        // char *rentalId = NULL;
        // rentalId = smalloc(sizeof(char), 8);
        int rentalId = -1;
        char *dongleKey = NULL;
        dongleKey = smalloc(sizeof(char), 33);
        char *rentInfo = NULL;
        rentInfo = smalloc(sizeof(char), 42);

        char **parsedRentInfo = NULL;

        clock_t startTime = 0;
        clock_t endTime = 0;
        float duration = 0;

        int helloResult = -256;

        HANDLE dongle;

        networkState = isConnected();

        if (initSql()) {
          printf("[+] Connecté à la bdd\n");
        }else{
          printf("[!] Impossible de se connecter à la bdd\n");
          execCmd(DISCONNECT);
          exit(1);
        }

        while (1) {
                startTime = clock();

                dongle = getDongleHandle(&helloResult);

                switch (helloResult) {
                case ERR_NOT_FOUND:
                        printf("[+] No dongle found.\n");
                        networkState = NETWORK_DOWN;
                        printf("[+] Disconnecting...\n");
                        execCmd(DISCONNECT);
                        break;
                case ERR_NOT_UNIQ:
                        printf("[+] Found more than 1 dongle. Only use one dongle at a time.\n");
                        networkState = NETWORK_DOWN;
                        printf("[+] Disconnecting...\n");
                        execCmd(DISCONNECT);
                        break;
                case DONGLE_FOUND:
                        // Fetch key
                        rentInfo = getKeyFromDongle(dongle);
                        parsedRentInfo = splitStr(rentInfo, ":", &splitCnt);

                        if (splitCnt < 2) {
                          printf("[!] Something went wrong\n");
                          networkState = NETWORK_DOWN;
                          printf("[+] Disconnecting...\n");
                          execCmd(DISCONNECT);
                          break;
                        }

                        // Extract id
                        rentalId = atoi(parsedRentInfo[0]);

                        if (rentalId < 0) {
                          printf("[!] Something went wrong\n");
                          networkState = NETWORK_DOWN;
                          printf("[+] Disconnecting...\n");
                          execCmd(DISCONNECT);
                          break;
                        }

                        // Extract key
                        strcpy(dongleKey, parsedRentInfo[1]);
                        printf("[>] Rental id extracted : %d\n", rentalId);
                        printf("[>] Key extracted : %s\n", dongleKey);

                        key = fetchAuthKey(rentalId);

                        printf("[>] Key fetched from database : %s\n", key);

                        // TODO
                        // Hash key when initializing dongle with python script
                        // Here hash DB key and compare it to retrieved dongleKey

                        if (strcmp(key, dongleKey) == 0) {
                          printf("[+] Key match !\n");
                          if (networkState == NETWORK_DOWN) {
                            printf("[+] Reconnecting...\n");
                            networkState = NETWORK_UP;
                            execCmd(TRY_CONNECT);
                          }else{
                            printf("[+] Back to sleep...\n");
                          }
                        }else{
                          printf("[+] Key does not match\n");
                          printf("[+] Disconnecting...\n");
                          networkState = NETWORK_DOWN;
                          execCmd(DISCONNECT);
                        }
                        // Check key with db
                        // If key match, then go back to sleep
                        // Else cut wifi
                        break;
                case ERR_UNEXPECTED:
                        printf("[+] Unexpected result while the hello broadcast.\n");
                        // Retry instantly ?
                        printf("[+] Disconnecting...\n");
                        networkState = NETWORK_DOWN;
                        execCmd(DISCONNECT);
                        break;
                }

                if (dongle != NULL && CloseHandle(dongle) == 0)
                {
                        printf("[+] Error while closing the handle.\n");
                        return 1;
                }

                dongle = NULL;

                endTime = clock();
                duration = getSeconds(startTime, endTime);

                if (duration < MAIN_LOOP_TIMEOUT) {
                        Sleep((MAIN_LOOP_TIMEOUT - duration)*1000);
                }
        }

        /*if (strcmp(bytes_recvd, key) == 0) {
                printf("key match\n");
           }else{
                printf("key does not match\n");
                system("bip.bat");
           }*/

        return 0;
}
