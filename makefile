CC=gcc
CFLAGS=-Wall
EXEC=dongled
# LINKER=-llibmysqlclient
LINKER=-s C:\MinGW\lib\libmysqlclient.a
OBJ=main.c lib/dongled.c lib/toolbox.c lib/db.c

all:	$(OBJ)
	cls && $(CC) $(CFLAGS) $(OBJ) $(LINKER) -o $(EXEC)

run:	$(OBJ)
	cls && $(CC) $(CFLAGS) $(OBJ) $(LINKER) -o $(EXEC) && ${EXEC}
